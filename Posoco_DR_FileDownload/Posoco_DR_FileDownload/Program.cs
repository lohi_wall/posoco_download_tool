﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Posoco_DR_FileDownload
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
            string yesterdayDate = DateTime.Now.Date.AddDays(-1).ToString("dd-MM-yy");
            string customUrl = "https://posoco.in/download/" + yesterdayDate + "_nldc_psp/";
            string customFileName = DateTime.Now.Date.AddDays(-1).ToString("dd.MM.yy") + "_NLDC_PSP";
            string DownloadFolder = @"C:\Posoco\Power_Supply\Inbound\";
            string pdfFolder = DownloadFolder + @"\PDF File\";
            string wordFolder = DownloadFolder + @"\Word File";
            string fullpdfFileName = DownloadFolder + "\\PDF File\\" + customFileName + ".pdf";
            int count = 0;
                HtmlWeb web = new HtmlWeb();
                HtmlDocument document = web.Load(customUrl);
                var anchorurl = document.DocumentNode.Descendants("a").First(x => x.Attributes["class"] != null && x.Attributes["class"].Value == "wpdm-download-link wpdm-download-locked [btnclass]");
                Match match = Regex.Match(anchorurl.Attributes["onclick"].Value, @"'([^']*)");
                string latestDocurl = "";
                if (match.Success)
                {
                    latestDocurl = match.Groups[1].Value;
                }
                if (!string.IsNullOrEmpty(latestDocurl))
                {
                    WebClient myWebClient = new WebClient();
                    if (File.Exists(fullpdfFileName))
                    {
                        File.Delete(fullpdfFileName);
                    }
                    myWebClient.DownloadFile(latestDocurl, fullpdfFileName);
                }
                string[] pdffiles = Directory.GetFiles(pdfFolder, "*.pdf");
                    if (pdffiles.Length == 0)
                    {
                        ExceptionLogging.SendNotFound("No Pdf File to Process");
                    }
                    foreach (var pdffile in pdffiles)
                    {
                    count++;
                        SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();
                        f.OpenPdf(pdffile);
                    if (f.PageCount > 0)
                    {
                        f.WordOptions.Format = SautinSoft.PdfFocus.CWordOptions.eWordDocument.Docx;
                        f.WordOptions.DetectTables = true;
                        string fullWordFileName = wordFolder + @"\" + Path.GetFileNameWithoutExtension(pdffile) + ".docx";
                            if (File.Exists(fullWordFileName))
                            {
                                File.Delete(fullWordFileName);
                            }
                        int result = f.ToWord(fullWordFileName);
                    }
                    Console.WriteLine(count + " file");
                }
        }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
        }
    }
}
