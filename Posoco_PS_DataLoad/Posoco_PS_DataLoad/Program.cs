﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Data;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Configuration;
using System.Globalization;

namespace Posoco_PS_DataLoad
{
    class Program
    {
        static void Main(string[] args)
        {
            string DownloadFolder = @"C:\Posoco\Power_Supply\Inbound\";
            string pdfFileFolder = DownloadFolder + "\\PDF File";
            string wordFileFolder = DownloadFolder + "\\Word File";
            string versionid = DateTime.Now.Date.ToString("yyyyMMdd");
            string archiveFolder = @"C:\Posoco\Power_Supply\Archive\" + versionid +@"\";
            string reportDate = "";
            string jobrun_date = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss tt");
            int count = 0;
            try 
            { 
            string[] docfiles = Directory.GetFiles(wordFileFolder,"*.docx");
            if (docfiles.Length == 0)
            {
                    ExceptionLogging.SendNotFound("No Word File to Process");
            }
                foreach (var docfile in docfiles)
                {
                    count++;
                    string pdfFile = Path.Combine(pdfFileFolder, Path.GetFileNameWithoutExtension(docfile) + ".pdf");
                    if ((!Directory.Exists(archiveFolder)) || (Directory.Exists(archiveFolder) && !File.Exists(Path.Combine(archiveFolder, Path.GetFileName(docfile)))))
                {
                    DataTable dt = new DataTable();
                        reportDate = DateTime.ParseExact((Path.GetFileNameWithoutExtension(docfile).Split('_')[0]).Replace('.', '-'), "dd-MM-yy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                        using (var doc = WordprocessingDocument.Open(docfile, false))
                    {
                        int rowCount = 0;
                        Table table = doc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(2);
                        IEnumerable<TableRow> rows = table.Elements<TableRow>();
                        foreach (TableRow row in rows)
                        {
                            if (rowCount == 0)
                            {
                                foreach (TableCell cell in row.Descendants<TableCell>())
                                {
                                    dt.Columns.Add(cell.InnerText);
                                }
                                rowCount += 1;
                            }
                            else
                            {
                                dt.Rows.Add();
                                int i = 0;
                                foreach (TableCell cell in row.Descendants<TableCell>())
                                {
                                    dt.Rows[dt.Rows.Count - 1][i] = cell.InnerText;
                                    i++;
                                }
                            }
                        }
                    }
                    loadDataToTable(dt, reportDate, jobrun_date);
                    if (!Directory.Exists(archiveFolder))
                    {
                        Directory.CreateDirectory(archiveFolder);
                    }
                    if (File.Exists(docfile) && File.Exists(pdfFile))
                    {
                        File.Move(docfile, Path.Combine(archiveFolder, Path.GetFileName(docfile)));
                        File.Move(pdfFile, Path.Combine(archiveFolder, Path.GetFileName(pdfFile)));
                    }
                }
                else
                    {
                        if (File.Exists(docfile) && File.Exists(pdfFile))
                        {
                            File.Delete(docfile);
                            File.Delete(pdfFile);
                        }
                        ExceptionLogging.SendNotFound("File is already Processed");
                    }
                    Console.WriteLine(count + "file");
            }
        }
        catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
        }
        public static void loadDataToTable(DataTable dt,string reportDate, string jobrun_date)
        {
            try 
            {
            string connStr = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
            MySqlConnection con = new MySqlConnection(connStr);
            MySqlCommand cmd;
            string region = "";
            con.Open();
            foreach (DataRow row in dt.Rows)
            {
                region = (string.IsNullOrEmpty(row[0].ToString())) ? region : row[0].ToString();
                    string query = "Insert INTO posoco_power_supply (region,state,max_demand,shortage,energy_met,drawal_schedule,od_ud,max_od,energy_shortage,report_date,jobrun_date) values ('" + region + "','" + row[1] + "'," + row[2] + "," + row[3] + ",'" + row[4] + "','" + row[5] + "','" + row[6] + "','" + row[7] + "','" + null + "','" + reportDate + "','" + jobrun_date + "')";
                    //string query = "Insert INTO posoco_power_supply (region,state,max_demand,shortage,energy_met,drawal_schedule,od_ud,max_od,energy_shortage,report_date,jobrun_date) values ('" + region + "','" + row[1] + "'," + row[2] + "," + row[3] + ",'" + row[4] + "','" + row[5] + "','" + row[6] + "','" + row[7] + "','" + row[8] + "','" + reportDate + "','" + jobrun_date + "')";
                    cmd = new MySqlCommand(query, con);
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }
        catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
}
    }
}
